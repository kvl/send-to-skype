[Files]
Source: Y:\Projects\SendToSkype\bin\Send2Skype.exe; DestDir: {app}; 
Source: Y:\Projects\SendToSkype\bin\l2s.htm; DestDir: {app}; 
Source: Y:\Projects\SendToSkype\bin\Skype4COM.dll; DestDir: {sys}; 
Source: Y:\Projects\SendToSkype\ThirdParty\licence_eng.txt; DestDir: {app}; 
Source: Y:\Projects\SendToSkype\ThirdParty\licence_rus.txt; DestDir: {app}; 
Source: Y:\Projects\SendToSkype\ThirdParty\help.html; DestDir: {app}; 

[Setup]
AppName=Send To Skype
DefaultDirName={pf}\SendToSkype
AppCopyright=(C) Alvosoft.com
AppVerName=0.1
OutputDir=Y:\Projects\SendToSkype\Output
OutputBaseFilename=setup_send2skype
ShowLanguageDialog=auto
DisableWelcomePage=true
DisableFinishedPage=true
DisableReadyPage=true
DisableProgramGroupPage=auto
DefaultGroupName=Alvosoft
VersionInfoVersion=0.1
VersionInfoCompany=Alvosoft.com
VersionInfoCopyright=Alvosoft.com
VersionInfoProductName=SendToSkype
VersionInfoProductVersion=0.1
SetupIconFile=Y:\Projects\SendToSkype\ThirdParty\Send2Skype_Icon.ico
UninstallLogMode=new
UninstallDisplayName=Send2Skype
AppPublisher=Alvosoft.com
AppVersion=0.1
UninstallDisplayIcon={app}\Send2Skype.exe
AlwaysShowComponentsList=false
ShowComponentSizes=false
FlatComponentsList=false

[InnoIDE_Settings]
UseRelativePaths=true

[Registry]
Root: HKCU; SubKey: "Software\Microsoft\Internet Explorer\MenuExt\Send to Skype"; ValueData: file://{app}\l2s.htm; ValueType: string; Flags: UninsDeleteKey; 
Root: HKCU; SubKey: "Software\Microsoft\Internet Explorer\MenuExt\Send to Skype"; ValueName: context; ValueData: 51; ValueType: dword; Flags: UninsDeleteKey; 

[InnoIDE_PostCompile]

[Run]
Filename: {app}\Send2Skype.exe; Parameters: /regserver; Flags: RunAsCurrentUser ShellExec WaitUntilIdle; 
Filename: {sys}\regsvr32.exe; Parameters: "{sys}\Skype4COM.dll /s"; Flags: RunAsCurrentUser ShellExec WaitUntilIdle PostInstall; 

[UninstallRun]
Filename: {app}\Send2Skype.exe; Parameters: /unregserver; Flags: ShellExec WaitUntilIdle;
Filename: regsvr32.exe; Parameters: "{sys}\Skype4COM.dll /s /u"; Flags: ShellExec WaitUntilIdle;

[Icons]
Name: {group}\; Filename: {app}\Send2Skype.exe; IconFilename: {app}\Send2Skype.exe; IconIndex: 0;
Name: {group}\; Filename: {app}\help.html;
Name: {group}\; Filename: {app}\licence_eng.txt;
Name: "{group}\{cm:UninstallProgram, Send To Skype}"; Filename: {uninstallexe};
