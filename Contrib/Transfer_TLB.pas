unit Transfer_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 06.07.2012 1:21:59 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Users\�������� ��������\�����\LinkToSkype\bin\Link2Skype.exe (1)
// LIBID: {B0BBE14E-2188-437A-AB66-C18749951BD7}
// LCID: 0
// Helpfile: 
// HelpString: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
// Errors:
//   Hint: TypeInfo 'Transfer' changed to 'Transfer_'
// ************************************************************************ //
// *************************************************************************//
// NOTE:                                                                      
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties  
// which return objects that may need to be explicitly created via a function 
// call prior to any access via the property. These items have been disabled  
// in order to prevent accidental use from within the object inspector. You   
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively   
// removing them from the $IFDEF blocks. However, such items must still be    
// programmatically created via a method of the appropriate CoClass before    
// they can be used.                                                          
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  TransferMajorVersion = 1;
  TransferMinorVersion = 0;

  LIBID_Transfer: TGUID = '{B0BBE14E-2188-437A-AB66-C18749951BD7}';

  IID_ITransfer: TGUID = '{3A8A9C0E-ABB9-4094-8725-5D2A264A5A91}';
  CLASS_Transfer_: TGUID = '{4BD7D25A-12AA-49A7-8462-593EEB438D1A}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ITransfer = interface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  Transfer_ = ITransfer;


// *********************************************************************//
// Interface: ITransfer
// Flags:     (256) OleAutomation
// GUID:      {3A8A9C0E-ABB9-4094-8725-5D2A264A5A91}
// *********************************************************************//
  ITransfer = interface(IUnknown)
    ['{3A8A9C0E-ABB9-4094-8725-5D2A264A5A91}']
    function URL(const Value: WideString): HResult; stdcall;
  end;

// *********************************************************************//
// The Class CoTransfer_ provides a Create and CreateRemote method to          
// create instances of the default interface ITransfer exposed by              
// the CoClass Transfer_. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoTransfer_ = class
    class function Create: ITransfer;
    class function CreateRemote(const MachineName: string): ITransfer;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TTransfer
// Help String      : Transfer
// Default Interface: ITransfer
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TTransferProperties= class;
{$ENDIF}
  TTransfer = class(TOleServer)
  private
    FIntf:        ITransfer;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TTransferProperties;
    function      GetServerProperties: TTransferProperties;
{$ENDIF}
    function      GetDefaultInterface: ITransfer;
  protected
    procedure InitServerData; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: ITransfer);
    procedure Disconnect; override;
    function URL(const Value: WideString): HResult;
    property DefaultInterface: ITransfer read GetDefaultInterface;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TTransferProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TTransfer
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TTransferProperties = class(TPersistent)
  private
    FServer:    TTransfer;
    function    GetDefaultInterface: ITransfer;
    constructor Create(AServer: TTransfer);
  protected
  public
    property DefaultInterface: ITransfer read GetDefaultInterface;
  published
  end;
{$ENDIF}


procedure Register;

resourcestring
  dtlServerPage = 'ActiveX';

  dtlOcxPage = 'ActiveX';

implementation

uses ComObj;

class function CoTransfer_.Create: ITransfer;
begin
  Result := CreateComObject(CLASS_Transfer_) as ITransfer;
end;

class function CoTransfer_.CreateRemote(const MachineName: string): ITransfer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Transfer_) as ITransfer;
end;

procedure TTransfer.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{4BD7D25A-12AA-49A7-8462-593EEB438D1A}';
    IntfIID:   '{3A8A9C0E-ABB9-4094-8725-5D2A264A5A91}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TTransfer.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as ITransfer;
  end;
end;

procedure TTransfer.ConnectTo(svrIntf: ITransfer);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TTransfer.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TTransfer.GetDefaultInterface: ITransfer;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TTransfer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TTransferProperties.Create(Self);
{$ENDIF}
end;

destructor TTransfer.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TTransfer.GetServerProperties: TTransferProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TTransfer.URL(const Value: WideString): HResult;
begin
  Result := DefaultInterface.URL(Value);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TTransferProperties.Create(AServer: TTransfer);
begin
  inherited Create;
  FServer := AServer;
end;

function TTransferProperties.GetDefaultInterface: ITransfer;
begin
  Result := FServer.DefaultInterface;
end;

{$ENDIF}

procedure Register;
begin
  RegisterComponents(dtlServerPage, [TTransfer]);
end;

end.
