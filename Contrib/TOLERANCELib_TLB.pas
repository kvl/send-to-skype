unit TOLERANCELib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : 1.2
// File generated on 06.07.2012 1:21:27 from Type Library described below.

// ************************************************************************  //
// Type Lib: C:\Program Files\Windows Media Player\mpvis.dll (1)
// LIBID: {C58F1580-0DF3-401C-93B1-2D9DDA61CF04}
// LCID: 0
// Helpfile: 
// HelpString: Tolerance 1.0 Type Library
// DepndLst: 
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
// Errors:
//   Error creating palette bitmap of (TToleranceVis) : Server C:\Program Files\Windows Media Player\mpvis.DLL contains no icons
// ************************************************************************ //
// *************************************************************************//
// NOTE:                                                                      
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties  
// which return objects that may need to be explicitly created via a function 
// call prior to any access via the property. These items have been disabled  
// in order to prevent accidental use from within the object inspector. You   
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively   
// removing them from the $IFDEF blocks. However, such items must still be    
// programmatically created via a method of the appropriate CoClass before    
// they can be used.                                                          
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, StdVCL, Variants;
  

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  TOLERANCELibMajorVersion = 1;
  TOLERANCELibMinorVersion = 0;

  LIBID_TOLERANCELib: TGUID = '{C58F1580-0DF3-401C-93B1-2D9DDA61CF04}';

  IID_IToleranceVis: TGUID = '{9A85D909-C64A-4608-8DC4-76254D869553}';
  CLASS_ToleranceVis: TGUID = '{0AA02E8D-F851-4CB0-9F64-BBA9BE7A983D}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IToleranceVis = interface;
  IToleranceVisDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  ToleranceVis = IToleranceVis;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  POleVariant1 = ^OleVariant; {*}


// *********************************************************************//
// Interface: IToleranceVis
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9A85D909-C64A-4608-8DC4-76254D869553}
// *********************************************************************//
  IToleranceVis = interface(IDispatch)
    ['{9A85D909-C64A-4608-8DC4-76254D869553}']
    function Get_foregroundColor: WideString; safecall;
    procedure Set_foregroundColor(const pVal: WideString); safecall;
    function Get_backgroundColor: WideString; safecall;
    procedure Set_backgroundColor(const pVal: WideString); safecall;
    function Get_miniMode: WideString; safecall;
    procedure Set_miniMode(const pVal: WideString); safecall;
    function Get_alpha: Integer; safecall;
    procedure Set_alpha(pVal: Integer); safecall;
    procedure GetProperty(const bstrName: WideString; out pvarProperty: OleVariant); safecall;
    procedure SetProperty(const bstrName: WideString; var pvarProperty: OleVariant); safecall;
    property foregroundColor: WideString read Get_foregroundColor write Set_foregroundColor;
    property backgroundColor: WideString read Get_backgroundColor write Set_backgroundColor;
    property miniMode: WideString read Get_miniMode write Set_miniMode;
    property alpha: Integer read Get_alpha write Set_alpha;
  end;

// *********************************************************************//
// DispIntf:  IToleranceVisDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9A85D909-C64A-4608-8DC4-76254D869553}
// *********************************************************************//
  IToleranceVisDisp = dispinterface
    ['{9A85D909-C64A-4608-8DC4-76254D869553}']
    property foregroundColor: WideString dispid 200;
    property backgroundColor: WideString dispid 203;
    property miniMode: WideString dispid 204;
    property alpha: Integer dispid 205;
    procedure GetProperty(const bstrName: WideString; out pvarProperty: OleVariant); dispid 201;
    procedure SetProperty(const bstrName: WideString; var pvarProperty: OleVariant); dispid 202;
  end;

// *********************************************************************//
// The Class CoToleranceVis provides a Create and CreateRemote method to          
// create instances of the default interface IToleranceVis exposed by              
// the CoClass ToleranceVis. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoToleranceVis = class
    class function Create: IToleranceVis;
    class function CreateRemote(const MachineName: string): IToleranceVis;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TToleranceVis
// Help String      : ToleranceVis: Not Public.  Internal object used by Windows Media Player.
// Default Interface: IToleranceVis
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TToleranceVisProperties= class;
{$ENDIF}
  TToleranceVis = class(TOleServer)
  private
    FIntf:        IToleranceVis;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TToleranceVisProperties;
    function      GetServerProperties: TToleranceVisProperties;
{$ENDIF}
    function      GetDefaultInterface: IToleranceVis;
  protected
    procedure InitServerData; override;
    function Get_foregroundColor: WideString;
    procedure Set_foregroundColor(const pVal: WideString);
    function Get_backgroundColor: WideString;
    procedure Set_backgroundColor(const pVal: WideString);
    function Get_miniMode: WideString;
    procedure Set_miniMode(const pVal: WideString);
    function Get_alpha: Integer;
    procedure Set_alpha(pVal: Integer);
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IToleranceVis);
    procedure Disconnect; override;
    procedure GetProperty(const bstrName: WideString; out pvarProperty: OleVariant);
    procedure SetProperty(const bstrName: WideString; var pvarProperty: OleVariant);
    property DefaultInterface: IToleranceVis read GetDefaultInterface;
    property foregroundColor: WideString read Get_foregroundColor write Set_foregroundColor;
    property backgroundColor: WideString read Get_backgroundColor write Set_backgroundColor;
    property miniMode: WideString read Get_miniMode write Set_miniMode;
    property alpha: Integer read Get_alpha write Set_alpha;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TToleranceVisProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TToleranceVis
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TToleranceVisProperties = class(TPersistent)
  private
    FServer:    TToleranceVis;
    function    GetDefaultInterface: IToleranceVis;
    constructor Create(AServer: TToleranceVis);
  protected
    function Get_foregroundColor: WideString;
    procedure Set_foregroundColor(const pVal: WideString);
    function Get_backgroundColor: WideString;
    procedure Set_backgroundColor(const pVal: WideString);
    function Get_miniMode: WideString;
    procedure Set_miniMode(const pVal: WideString);
    function Get_alpha: Integer;
    procedure Set_alpha(pVal: Integer);
  public
    property DefaultInterface: IToleranceVis read GetDefaultInterface;
  published
    property foregroundColor: WideString read Get_foregroundColor write Set_foregroundColor;
    property backgroundColor: WideString read Get_backgroundColor write Set_backgroundColor;
    property miniMode: WideString read Get_miniMode write Set_miniMode;
    property alpha: Integer read Get_alpha write Set_alpha;
  end;
{$ENDIF}


procedure Register;

resourcestring
  dtlServerPage = 'ActiveX';

  dtlOcxPage = 'ActiveX';

implementation

uses ComObj;

class function CoToleranceVis.Create: IToleranceVis;
begin
  Result := CreateComObject(CLASS_ToleranceVis) as IToleranceVis;
end;

class function CoToleranceVis.CreateRemote(const MachineName: string): IToleranceVis;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_ToleranceVis) as IToleranceVis;
end;

procedure TToleranceVis.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{0AA02E8D-F851-4CB0-9F64-BBA9BE7A983D}';
    IntfIID:   '{9A85D909-C64A-4608-8DC4-76254D869553}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TToleranceVis.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IToleranceVis;
  end;
end;

procedure TToleranceVis.ConnectTo(svrIntf: IToleranceVis);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TToleranceVis.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TToleranceVis.GetDefaultInterface: IToleranceVis;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TToleranceVis.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TToleranceVisProperties.Create(Self);
{$ENDIF}
end;

destructor TToleranceVis.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TToleranceVis.GetServerProperties: TToleranceVisProperties;
begin
  Result := FProps;
end;
{$ENDIF}

function TToleranceVis.Get_foregroundColor: WideString;
begin
    Result := DefaultInterface.foregroundColor;
end;

procedure TToleranceVis.Set_foregroundColor(const pVal: WideString);
  { Warning: The property foregroundColor has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.foregroundColor := pVal;
end;

function TToleranceVis.Get_backgroundColor: WideString;
begin
    Result := DefaultInterface.backgroundColor;
end;

procedure TToleranceVis.Set_backgroundColor(const pVal: WideString);
  { Warning: The property backgroundColor has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.backgroundColor := pVal;
end;

function TToleranceVis.Get_miniMode: WideString;
begin
    Result := DefaultInterface.miniMode;
end;

procedure TToleranceVis.Set_miniMode(const pVal: WideString);
  { Warning: The property miniMode has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.miniMode := pVal;
end;

function TToleranceVis.Get_alpha: Integer;
begin
    Result := DefaultInterface.alpha;
end;

procedure TToleranceVis.Set_alpha(pVal: Integer);
begin
  DefaultInterface.Set_alpha(pVal);
end;

procedure TToleranceVis.GetProperty(const bstrName: WideString; out pvarProperty: OleVariant);
begin
  DefaultInterface.GetProperty(bstrName, pvarProperty);
end;

procedure TToleranceVis.SetProperty(const bstrName: WideString; var pvarProperty: OleVariant);
begin
  DefaultInterface.SetProperty(bstrName, pvarProperty);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TToleranceVisProperties.Create(AServer: TToleranceVis);
begin
  inherited Create;
  FServer := AServer;
end;

function TToleranceVisProperties.GetDefaultInterface: IToleranceVis;
begin
  Result := FServer.DefaultInterface;
end;

function TToleranceVisProperties.Get_foregroundColor: WideString;
begin
    Result := DefaultInterface.foregroundColor;
end;

procedure TToleranceVisProperties.Set_foregroundColor(const pVal: WideString);
  { Warning: The property foregroundColor has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.foregroundColor := pVal;
end;

function TToleranceVisProperties.Get_backgroundColor: WideString;
begin
    Result := DefaultInterface.backgroundColor;
end;

procedure TToleranceVisProperties.Set_backgroundColor(const pVal: WideString);
  { Warning: The property backgroundColor has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.backgroundColor := pVal;
end;

function TToleranceVisProperties.Get_miniMode: WideString;
begin
    Result := DefaultInterface.miniMode;
end;

procedure TToleranceVisProperties.Set_miniMode(const pVal: WideString);
  { Warning: The property miniMode has a setter and a getter whose
    types do not match. Delphi was unable to generate a property of
    this sort and so is using a Variant as a passthrough. }
var
  InterfaceVariant: OleVariant;
begin
  InterfaceVariant := DefaultInterface;
  InterfaceVariant.miniMode := pVal;
end;

function TToleranceVisProperties.Get_alpha: Integer;
begin
    Result := DefaultInterface.alpha;
end;

procedure TToleranceVisProperties.Set_alpha(pVal: Integer);
begin
  DefaultInterface.Set_alpha(pVal);
end;

{$ENDIF}

procedure Register;
begin
  RegisterComponents(dtlServerPage, [TToleranceVis]);
end;

end.
