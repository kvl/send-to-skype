unit Transfer;

{$WARN SYMBOL_PLATFORM OFF}

interface

uses
  ComObj, ActiveX, Send2Skype_TLB, StdVcl, Main, Forms;

type
  TTransfer = class(TAutoObject, ITransfer)
  private
    FURL: string;
    FSelectedText: string;
    FDocURL: string;
    function GenerateMessage: string;
  protected
    procedure SelectedText(const Value: WideString); safecall;
    procedure URL(const Value: WideString); safecall;
    procedure DocURL(const Value: WideString); safecall;
    procedure Go; safecall;
    procedure QGo(const DocURL, URL, SelectedText: WideString); safecall;

  end;

implementation

uses ComServ;

procedure TTransfer.SelectedText(const Value: WideString);
begin
  FSelectedText := Value;
end;

procedure TTransfer.URL(const Value: WideString);
begin
  FURL := Value;
end;

procedure TTransfer.DocURL(const Value: WideString);
begin
  FDocURL := Value;
end;

function TTransfer.GenerateMessage: string;
begin
  if FSelectedText <> '' then
    Result := FSelectedText
  else
    Result := '';

  if FURL <> '' then begin
    if Result <> '' then Result := Result + ': ';
    Result := Result + FURL;
  end else if FDocURL <> '' then begin
    if Result <> '' then Result := Result + ': ';
    Result := Result + FDocURL;
  end;
end;

procedure TTransfer.Go;
var
  s: string;
begin
  s := GenerateMessage;
  MainForm.txtMessage.Lines.Add(s);
  Application.BringToFront;
end;

procedure TTransfer.QGo(const DocURL, URL, SelectedText: WideString);
begin
  FDocURL := DocURL;
  FURL := URL;
  FSelectedText := SelectedText;
  Go;
end;

initialization
  TAutoObjectFactory.Create(ComServer, TTransfer, Class_Transfer,
    ciMultiInstance, tmApartment);
  CoAddRefServerProcess;

end.
