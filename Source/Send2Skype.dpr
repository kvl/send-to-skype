program Send2Skype;

uses
  Forms,
  Main in 'Main.pas' {MainForm},
  SKYPE4COMLib_TLB in '..\Contrib\SKYPE4COMLib_TLB.pas',
  Transfer in 'Transfer.pas' {Transfer: CoClass};

{$R *.TLB}

{$R *.res}

begin
  Forms.Application.Initialize;
  Forms.Application.MainFormOnTaskbar := True;
  Forms.Application.CreateForm(TMainForm, MainForm);
  Forms.Application.Run;
end.
