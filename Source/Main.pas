unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, SKYPE4COMLib_TLB, StdCtrls, Grids, Vcl.ComCtrls, ShellAPI, ShlObj,
  Generics.Collections;

const
  constMRUListFileName = 'MRUList.txt';
  constSettingsDir = 'Send2Skype';
  constFavName = 'Favorites';

type
  TMainForm = class(TForm)
    txtMessage: TMemo;
    UserList: TListView;
    btSend: TButton;
    btHelp: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btSendClick(Sender: TObject);
    procedure UserListSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure UserListKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure txtMessageKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btHelpClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    sk: TSkype;
    SelectedUser: string;
    SelectedUserDispName: string;
    FMRUList: TStringList;
    function GetMRUListPath: string;
    function FormName(ACol: IUserCollection; Index: integer): string;
    procedure FillUser;
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.btHelpClick(Sender: TObject);
var
  s: string;
begin
  s := ExtractFilePath(Forms.Application.ExeName) + 'help.html';
  ShellExecute(0, 'open', PChar(s), '', '', SW_SHOW);
end;

procedure TMainForm.btSendClick(Sender: TObject);
var
  chat: IChat;
begin
  if SelectedUser = '' then exit;
  if txtMessage.Text = '' then exit;
  FMRUList.Values[SelectedUser] := SelectedUserDispName;
  chat := sk.CreateChatWith(SelectedUser);
  if chat <> nil then begin
    chat.SendMessage(txtMessage.Text);
    txtMessage.Clear;
    chat := nil;
    Close;
  end;
end;

procedure TMainForm.FillUser;
var
  uc: IUserCollection;
  i, j: integer;
  s: string;
  t: TListItem;
  g: TListGroup;
begin

  g := UserList.Groups.Add;
  g.Subtitle := constFavName;
  for j := FMRUList.Count - 1 downto 0 do begin
    t := UserList.Items.Add;
    t.GroupID := g.GroupID;
    t.SubItems.Add(FMRUList.Names[j]);
    t.Caption := FMRUList.Values[t.SubItems[0]];
  end;

  for j := 1 to sk.Groups.Count do begin
    if not sk.Groups.Item[j].IsVisible then continue;
    g := UserList.Groups.Add;
    g.Subtitle := sk.Groups.Item[j].DisplayName;
    uc := sk.Groups.Item[j].Users;
    for i := 1 to uc.Count do begin
      if uc.Item[i].IsBlocked then continue;
      t := UserList.Items.Add;
      t.GroupID := g.GroupID;
      t.Caption := FormName(uc, i);
      t.SubItems.Add(uc.Item[i].Handle);
    end;
  end;
end;

procedure TMainForm.FormCreate(Sender: TObject);
begin
  sk := TSkype.Create(self);
  SelectedUser := '';
  SelectedUserDispName := '';
  FMRUList := TStringList.Create;
  if FileExists(GetMRUListPath) then
    FMRUList.LoadFromFile(GetMRUListPath);
//  FMRUList.Sorted := true;
  FMRUList.Duplicates := dupIgnore;
  FillUser;
end;

procedure TMainForm.FormDestroy(Sender: TObject);
begin
  FMRUList.Sort;
  FMRUList.SaveToFile(GetMRUListPath);
  FMRUList.Destroy;
end;

function TMainForm.FormName(ACol: IUserCollection; Index: integer): string;
begin
  Result := ACol.Item[Index].DisplayName;
  if Result = '' then Result := ACol.Item[Index].FullName;
  Result := Result + ' (' + ACol.Item[Index].Handle + ')';
end;

function TMainForm.GetMRUListPath: string;
var
  PIDL: PItemIDList;
  InFolder: array[0..MAX_PATH] of Char;
begin
  SHGetSpecialFolderLocation(Forms.Application.Handle, CSIDL_LOCAL_APPDATA, PIDL);
  SHGetPathFromIDList(PIDL, InFolder);
  Result := InFolder;
  Result := Result + '\' + constSettingsDir;
  if not DirectoryExists(Result) then
    CreateDir(Result);
  Result := Result + '\' + constMRUListFileName;
end;

procedure TMainForm.txtMessageKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = 13) and (ssCtrl in Shift) and
     (SelectedUser <> '') and (txtMessage.Text <> '') then
  begin
    btSendClick(Sender);
  end;
end;

procedure TMainForm.UserListKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = 13) and (UserList.Selected <> nil) then begin
    SelectedUser := UserList.Selected.SubItems[0];
    if txtMessage.Text = '' then
      txtMessage.SetFocus
    else
      btSendClick(Sender);
  end else if Key = VK_ESCAPE then begin
    Close;
  end;
end;

procedure TMainForm.UserListSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
begin
  SelectedUser := Item.SubItems[0];
  SelectedUserDispName := Item.Caption;
end;

end.
