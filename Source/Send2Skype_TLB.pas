unit Send2Skype_TLB;

// ************************************************************************ //
// WARNING
// -------
// The types declared in this file were generated from data read from a
// Type Library. If this type library is explicitly or indirectly (via
// another type library referring to this type library) re-imported, or the
// 'Refresh' command of the Type Library Editor activated while editing the
// Type Library, the contents of this file will be regenerated and all
// manual modifications will be lost.
// ************************************************************************ //

// $Rev: 45604 $
// File generated on 04.08.2012 0:07:35 from Type Library described below.

// ************************************************************************  //
// Type Lib: X:\SendToSkype\Source\Send2Skype (1)
// LIBID: {F21B4C11-4484-4D27-AC0C-1C7CFE067AF8}
// LCID: 0
// Helpfile:
// HelpString:
// DepndLst:
//   (1) v2.0 stdole, (C:\Windows\system32\stdole2.tlb)
//   (2) v4.0 StdVCL, (stdvcl40.dll)
// SYS_KIND: SYS_WIN32
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers.
{$WARN SYMBOL_PLATFORM OFF}
{$WRITEABLECONST ON}
{$VARPROPSETTER ON}
{$ALIGN 4}

interface

uses Winapi.Windows, System.Classes, System.Variants, System.Win.StdVCL, Vcl.Graphics, Vcl.OleServer, Winapi.ActiveX;


// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:
//   Type Libraries     : LIBID_xxxx
//   CoClasses          : CLASS_xxxx
//   DISPInterfaces     : DIID_xxxx
//   Non-DISP interfaces: IID_xxxx
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  Send2SkypeMajorVersion = 1;
  Send2SkypeMinorVersion = 0;

  LIBID_Send2Skype: TGUID = '{F21B4C11-4484-4D27-AC0C-1C7CFE067AF8}';

  IID_ITransfer: TGUID = '{132B379D-778B-46AE-BE2E-998A913CB062}';
  CLASS_Transfer: TGUID = '{0D309A7C-4145-4D29-95F5-096123895E6F}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary
// *********************************************************************//
  ITransfer = interface;
  ITransferDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library
// (NOTE: Here we map each CoClass to its Default Interface)
// *********************************************************************//
  Transfer = ITransfer;


// *********************************************************************//
// Interface: ITransfer
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {132B379D-778B-46AE-BE2E-998A913CB062}
// *********************************************************************//
  ITransfer = interface(IDispatch)
    ['{132B379D-778B-46AE-BE2E-998A913CB062}']
    procedure URL(const Value: WideString); safecall;
    procedure SelectedText(const Value: WideString); safecall;
    procedure DocURL(const Value: WideString); safecall;
    procedure Go; safecall;
    procedure QGo(const DocURL: WideString; const URL: WideString; const SelectedText: WideString); safecall;
  end;

// *********************************************************************//
// DispIntf:  ITransferDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {132B379D-778B-46AE-BE2E-998A913CB062}
// *********************************************************************//
  ITransferDisp = dispinterface
    ['{132B379D-778B-46AE-BE2E-998A913CB062}']
    procedure URL(const Value: WideString); dispid 201;
    procedure SelectedText(const Value: WideString); dispid 202;
    procedure DocURL(const Value: WideString); dispid 203;
    procedure Go; dispid 204;
    procedure QGo(const DocURL: WideString; const URL: WideString; const SelectedText: WideString); dispid 205;
  end;

// *********************************************************************//
// The Class CoTransfer provides a Create and CreateRemote method to
// create instances of the default interface ITransfer exposed by
// the CoClass Transfer. The functions are intended to be used by
// clients wishing to automate the CoClass objects exposed by the
// server of this typelibrary.
// *********************************************************************//
  CoTransfer = class
    class function Create: ITransfer;
    class function CreateRemote(const MachineName: string): ITransfer;
  end;

implementation

uses System.Win.ComObj;

class function CoTransfer.Create: ITransfer;
begin
  Result := CreateComObject(CLASS_Transfer) as ITransfer;
end;

class function CoTransfer.CreateRemote(const MachineName: string): ITransfer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_Transfer) as ITransfer;
end;

end.

