object MainForm: TMainForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Send to Skype'
  ClientHeight = 273
  ClientWidth = 683
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object txtMessage: TMemo
    Left = 344
    Top = 8
    Width = 329
    Height = 226
    ScrollBars = ssVertical
    TabOrder = 1
    OnKeyDown = txtMessageKeyDown
  end
  object UserList: TListView
    Left = 8
    Top = 8
    Width = 329
    Height = 257
    Columns = <
      item
        AutoSize = True
        Caption = 'Name'
      end>
    GridLines = True
    Groups = <
      item
        GroupID = 0
        State = [lgsNormal]
        HeaderAlign = taLeftJustify
        FooterAlign = taLeftJustify
        Subtitle = 'Recent'
        TitleImage = -1
      end
      item
        GroupID = 1
        State = [lgsNormal]
        HeaderAlign = taLeftJustify
        FooterAlign = taLeftJustify
        Subtitle = 'All'
        TitleImage = -1
      end>
    GroupView = True
    ReadOnly = True
    RowSelect = True
    SortType = stText
    TabOrder = 0
    ViewStyle = vsReport
    OnDblClick = btSendClick
    OnKeyUp = UserListKeyUp
    OnSelectItem = UserListSelectItem
  end
  object btSend: TButton
    Left = 510
    Top = 240
    Width = 75
    Height = 25
    Caption = 'Send'
    Default = True
    TabOrder = 2
    OnClick = btSendClick
  end
  object btHelp: TButton
    Left = 603
    Top = 240
    Width = 75
    Height = 25
    Caption = 'Help'
    TabOrder = 3
    OnClick = btHelpClick
  end
end
